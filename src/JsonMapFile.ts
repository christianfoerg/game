export interface JsonMapFile {
  compressionlevel: number;
  height: number;
  infinite: boolean;
  layers: Array<Layer>;
  nextlayerid: number;
  nextobjectid: number;
  orientation: string;
  renderorder: string;
  tiledversion: string;
  tileheight: number;
  tilesets: Array<TileSet>;
  tilewidth: number;
  type: string;
  version: string;
  width: number;
}

export interface Layer {
  data: Array<number>;
  height: number;
  id: number;
  name: string;
  opacity: number;
  type: string;
  visible: boolean;
  width: number;
  x: number;
  y: number;
}

interface TileSet {
  firstgid: number;
  source: string;
}
