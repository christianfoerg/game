export class Matrix<T> {
  constructor(private readonly data: T[][]) {}
  get(x: number, y: number): T {
    const row = this.data[y];
    return row ? row[x] : null;
  }
}
